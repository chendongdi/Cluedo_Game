package dom;

import java.util.List;
// Storing the pawns information
public class Pawns {
	private String type;    // pawn type
	private String name;    // pawn name
	private int X;          //  coordinate x
	private int Y;          // coordinate y
	
	public Pawns(String type, String name, int x, int y) {
		this.type = type;
		this.name = name;
		this.X = x;
		this.Y = y;
	}
	
	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getX() {
		return X;
	}

	public void setX(int x) {
		X = x;
	}

	public int getY() {
		return Y;
	}

	public void setY(int y) {
		Y = y;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
