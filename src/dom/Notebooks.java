package dom;

import java.util.List;
// Storing All the events of the game
public class Notebooks {
	private  List<String> rooms ;  //  room cards list
	private  List<String> suspects ; //suspects cards list
	private  List<String> weapons ;//weapons cards list
	private  List<String> hypothesis ;//hypothesis cards list
	
	public List<String> getRooms() {
		return rooms;
	}

	public void setRooms(List<String> rooms) {
		this.rooms = rooms;
	}

	public List<String> getSuspects() {
		return suspects;
	}

	public void setSuspects(List<String> suspects) {
		this.suspects = suspects;
	}

	public List<String> getWeapons() {
		return weapons;
	}

	public void setWeapons(List<String> weapons) {
		this.weapons = weapons;
	}

	public List<String> getHypothesis() {
		return hypothesis;
	}

	public void setHypothesis(List<String> hypothesis) {
		this.hypothesis = hypothesis;
	}

	public void setHypothesis() {
		
	}
	
	
}
