package dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// Cards is the library of all cards info
public abstract class Cards {
	public static final  String[] WEAPONS = {Weapon.CANDLESTICK, Weapon.KNIFE, Weapon.LEADPIPE, Weapon.POISON, Weapon.REVOLVER, Weapon.ROPE};
	public static final String[] SUSPECTS = {Suspects.GREEN, Suspects.Mustard, Suspects.PEACOCK, Suspects.PLUM, Suspects.SCARLETT, Suspects.WHITE};
	public static final String[] ROOMS = {Room.KITCHEN,Room.BALLROOM,Room.CONSERVATORY,Room.DININGROOM,Room.LOUNGE,Room.HALL,Room.STUDY, Room.BILLIARD,Room.LIBRARY,  };
	
	
//	Kitchen, Ballroom, Conservatory, Dining Room, Lounge,
//	Hall, Study, Billiard Room and Library
	
	//Map<String, String[]> cards = new HashMap<String, String[]>();
	
	public static Map<Integer, String> getRoomMap(Map<Integer, String> roomMap ) {
		
				roomMap.put(1, Cards.ROOMS[0]);
				roomMap.put(2, Cards.ROOMS[1]);
				roomMap.put(3, Cards.ROOMS[2]);
				roomMap.put(4, Cards.ROOMS[3]);
				roomMap.put(5, Cards.ROOMS[4]);
				roomMap.put(6, Cards.ROOMS[5]);
				roomMap.put(7, Cards.ROOMS[6]);
				roomMap.put(8, Cards.ROOMS[7]);
				roomMap.put(9, Cards.ROOMS[8]);
				
				return roomMap;
			}
			
		
	
	
	//method used to get all cards
	public static Map<String, String[]> getAllCards(){
		Map<String, String[]> cards = new HashMap<String, String[]>();
		
		cards.put("SUSPECTS", SUSPECTS);
		cards.put("WEAPONS", WEAPONS);
		cards.put("ROOMS", ROOMS);
		
		return cards;
	}
	
	
	
	
	
	
}
