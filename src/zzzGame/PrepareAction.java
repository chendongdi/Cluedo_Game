package zzzGame;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


import dom.Card;
import dom.Cards;
import dom.Notebooks;

public class PrepareAction {

	Map<String, List<String>> cardsIniLeft = new HashMap<String, List<String>>();   //store the cards except the initial three cards
	
	
	
	public   Map<Integer,Player> createPlayer() {

		
		 Scanner sc = new Scanner(System.in);   //get input from user
		 
	
		System.out.println("▇▇--------------------  Character Selection  ----------------------▇▇");


		System.out.println("Please insert the number of players(Range between 3-6)");
		System.out.print("△--");
		String line = sc.nextLine();    //input the number of players
		//check the valid of user input
		 while(!checkInteger(line,3,6)) {
			 System.out.println("Please insert the number of players(Range between 3-6)");
			 System.out.print("△--");
			 line = sc.nextLine();
		 }
		 int num  = Integer.valueOf(line);
		
		 
		List<String> suspects = asList(Cards.SUSPECTS);  //convert from array to list
		Map<Integer,Player> playerMap = new HashMap<Integer,Player>();
		
       // each player choose characters 
		for (int i = 1; i <= num; i++) {
			System.out.println("▇-Player "+i+" Please Choose Characters:");
			
			for (int j = 1; j <= suspects.size(); j++) {
				System.out.print(j + "-" + suspects.get(j-1) + "\t");
			}
			System.out.println();
			System.out.print("△--");
			String chaLine = sc.nextLine();
			//check valid of user input
			 while(!checkInteger(chaLine,1,suspects.size())) {
			
				 System.out.print("△--");
				 chaLine = sc.nextLine();
			 }
			int cha = Integer.valueOf(chaLine);
			String name = suspects.remove(cha-1);
			Player player = new Player(i,name);
			
			playerMap.put(i, player);
			System.out.println("▇---Player"+ i + " has choose " + name );
			System.out.println();
		
		}
		

		System.out.println("▇-ALL Players have chose characters");
		//show characters chosed by each player
		for (int i = 1; i <=playerMap.values().size() ; i++) {
			System.out.println("Player" + i+":\t" + playerMap.get(i).getName() );
		}
		
		System.out.println("★ ---------------- Press any with Enter to continue ----------------★");
		sc.next();
		clearL() ;
		
		return playerMap;
	}
	
	// This method used to check the valid of user input
	public boolean checkInteger(String line,int i, int j) {
		try {
		int num=Integer.valueOf(line);//把字符串强制转换为数字
		if(num>=i && num<=j) {
			return true;//如果是数字，返回True
		}	
		System.out.println("▇===== Your input is not valid=====▇ ");
		return false;
		} catch (Exception e) {
		System.out.println("▇===== Your input is not valid=====▇ ");
		return false;//如果抛出异常，返回False}
	}
	}
	
	// This method used to clear console.
	public  void clearL() 
    {
        Robot r;
		try {
			r = new Robot();
			  r.mousePress(InputEvent.BUTTON3_MASK);       
		        r.mouseRelease(InputEvent.BUTTON3_MASK);   
		        r.keyPress(KeyEvent.VK_CONTROL);             
		        r.keyPress(KeyEvent.VK_R);                   
		        r.keyRelease(KeyEvent.VK_R);                  
		        r.keyRelease(KeyEvent.VK_CONTROL);            
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      
       

    }
	
	// Method used to randomly select three cards
	public List<Card> getInitialCards(){
		
		List<String> weapons = asList(Cards.WEAPONS);   //get all weapons cards
		List<String> rooms =asList( Cards.ROOMS);       //get all rooms cards
		List<String> suspects = asList(Cards.SUSPECTS);   //get all suspects cards
		
		int index=(int)(Math.random()*weapons.size());   //get a random integer
		int index2=(int)(Math.random()*rooms.size());
		int index3=(int)(Math.random()*suspects.size());
		
		Card cardWeapons = new Card("weapon", weapons.remove(index) ) ;     //Card used to store the selected cards
		Card cardRooms = new Card("room",rooms.remove(index2) ) ;
		Card cardSuspects = new Card("suspect", suspects.remove(index3));

		List<Card> list = new ArrayList<Card>() ;// first three cards;
		list.add(cardWeapons);
		list.add(cardRooms);
		list.add(cardSuspects);
		
		
		cardsIniLeft.put("weapon", weapons);  //left cards after remove first three cards; 
		cardsIniLeft.put("rooms", rooms);
		cardsIniLeft.put("suspects", suspects);
		
		
		return list;
	}
	
	public Map<String, List<String>> getInitialLeft(){
		return cardsIniLeft;
	}
	
	public List<String> asList(String[] x){
		List<String> list = new ArrayList<String>();
		for(String y:x) {
			list.add(y);
		}
		return list;
	}
	
	//Method used to dealing the cards 
	public void deal(Map<String, List<String>> cards,Map<Integer,Player> playerMap) {
		int order = 1; //a variable used to deal to player 
		while(!cards.isEmpty()) {// loop until all cards are dealed. 
			
			Set<String> set = cards.keySet();  //get all card types;
			List<String> types = new ArrayList<String>(set);//gain key list
			int index=(int)(Math.random()*types.size());// 生成范围内的随机数
			String typeName = types.get(index);
			
			List<String> typeList = cards.get(typeName);  // get type value from map using typeName;
			
			int index1=(int)(Math.random()*typeList.size());
			String name = typeList.remove(index1);//select the card will be dealed next
			if(typeList.size()==0) {// judge whether there is card remaining in the certain type
				cards.remove(typeName);
			}
			
			if(order> playerMap.keySet().size()) {
				order=1;
			}
			
			Notebooks notebook = playerMap.get(order).getNotebook();
			if(typeName.equals("suspects")) {
				List<String> list = notebook.getSuspects();
				if(list==null) {
					 list = new ArrayList<String>();//if it is the first card in the list, then create list first
				}
				list.add(name);
				notebook.setSuspects(list); //put the new list in notebook(upgrade)
				order++;
			//	players.get(order++).setNotebook(notebook);// put the notebook into player
			}else if(typeName.equals("weapon")) {
				List<String> list = notebook.getWeapons();
				
				if(list==null) {
					 list = new ArrayList<String>();
				}
				list.add(name);
				notebook.setWeapons(list);
				order++;
			//	players.get(order++).setNotebook(notebook);
			}else if(typeName.equals("rooms")) {
				List<String> list = notebook.getRooms();
				if(list==null) {
					 list = new ArrayList<String>();
				}
				list.add(name);
				notebook.setRooms(list);
				order++;
			//	players.get(order++).setNotebook(notebook);
			}
			
		}
		
		
	}
	
	
}
