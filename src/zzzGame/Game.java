package zzzGame;

import java.util.List;
import java.util.Map;

import dom.Card;


// The main class of the game
public class Game {

	public void playGame() {

		PrepareAction prepare = new PrepareAction();
		Map<Integer,Player> playerMap = prepare.createPlayer();  //create player
		
		System.out.println("�~�~------------------------ Now Game Start -------------------------�~�~");
		System.out.println("�~-Dealing the cards:------");

		List<Card> initialCards = prepare.getInitialCards();  //get initial three cards

		Map<String, List<String>> mapRestCards = prepare.getInitialLeft();  //get all cards except the initial three cards
		prepare.deal(mapRestCards, playerMap); // deal, <cards,player>   //dealing the cards to all player
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("�~-All Cards have been dealing\n");
		
		
		
		GameAction gameAction = new GameAction();

		try {
			Thread.sleep(2000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gameAction.playGame(playerMap, initialCards,mapRestCards);   //detailed game logic
		
	}

	// THe entrance of game
	public static void main(String[] args) {

		Game game = new Game();
		System.out.println("---------------------------------------------------------------------");
		System.out.println("--------------------------     Cluedo    ----------------------------");
		System.out.println("---------------------------------------------------------------------");
		System.out.println();
		game.playGame();
		//game.showInfo(play);
	}



}
