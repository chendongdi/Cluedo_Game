package zzzGame;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import dom.Card;
import dom.Cards;
import dom.Notebooks;

//The main game logic 
public class GameAction {

	public Map<Integer, String> roomMap = new HashMap<Integer, String>();  //get the map between room number and room
	Scanner sc = new Scanner(System.in);

	public void playGame(Map<Integer, Player> players, List<Card> initialCards,
			Map<String, List<String>> mapRestCards) {

		boolean end = false; // used to stop game
		int allPlayerSize = players.size(); // get the total number of players
		int playerId = 1;
		roomMap = Cards.getRoomMap(roomMap); // get all room information
		List<Integer> delPlayer = new ArrayList<Integer>();

		while (!end) {   //loop until end is true;

			try {
				Thread.sleep(1000L);   
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("▇============  Player " + (playerId) + "(" + players.get(playerId).getName()
					+ ") is playing  =============▇");
			Board board = new Board();

			board.drawBoard(players.get(playerId)); // draw game board for playerId player
			System.out.println("======================================================================");

			try {
				Thread.sleep(1000L);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards   3-Play Dicing   ");
			System.out.print("△--");
			Scanner sa = new Scanner(System.in);
			String norPLine = sa.next();

			while (!checkInteger(norPLine, 1, 3)) { // check the valid of user input
				System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards   3-Play Dicing   ");
				System.out.print("△--");
				norPLine = sa.next();
			}

			int norP = Integer.valueOf(norPLine);  // parse input(string) to integer

			while (norP != 3) { // loop until user select play dicing
				if (norP == 1) {
					showMyNoteBook(players.get(playerId)); // show the notebook info
				} else {
					showMyCards(players.get(playerId)); // show cards info
				}
				System.out.println("Please choose: 1-Get my notebook   2-Get my cards   3-Play Dicing   ");
				System.out.print("△--");
				norPLine = sa.next();

				while (!checkInteger(norPLine, 1, 3)) { // check the valid of user input
					System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards   3-Play Dicing   ");
					System.out.print("△--");
					norPLine = sa.next();
				}
				norP = Integer.valueOf(norPLine);// parse input(string) to integer


			}

			int total = this.playDice(); // play dicing

			// ask user move or not
			int mv = judgeMove();
			while (mv > 2 || mv < 1) {
				System.out.println("▇===== Your input is not valid=====▇ ");
				mv = judgeMove();
			}
			int resultMV = 0;

			if (mv == 1) { // mv==1 meands user want to move
				resultMV = move(total, players.get(playerId));

				while (resultMV == -1) {
					System.out.println("▇===== Your input is not valid=====▇ ");
					resultMV = move(total, players.get(playerId));
				}

				System.out.println("▇ ==============After Moving===========▇ ");
				Board board2 = new Board();
				board2.drawBoard(players.get(playerId)); // show the user pawn position after moving
			} else {
				// mv=2 means user don't want to move
				Board board2 = new Board();
				int[][] b = board2.getBoard();
				resultMV = b[players.get(playerId).getPawn().getX()][players.get(playerId).getPawn().getY()];
			}

			if (resultMV > 100) { // if resultMV > 100 means user has entered in a room
				int f = resultMV / 100;
				System.out.println("▇-Now you are in the '" + roomMap.get(f) + "'space");
				if (f != 1 && f != 6 && f != 3 && f != 8) { 
					System.out.println("▇-Please choose: 1 do hypothesis, 2 do accuse");
					Scanner sb = new Scanner(System.in);
					System.out.print("△--");
					String hypothesisOrAccuseLine = sb.next();
					// check the valid of user
					while (!hypothesisOrAccuseLine.equals("1") && !hypothesisOrAccuseLine.equals("2")) {
						System.out.println("▇===== Your input is not valid=====▇ ");
						System.out.println("Please choose: 1 do hypothesis, 2 do accuse ,3 get");
						hypothesisOrAccuseLine = sb.next();
					}
					boolean accuseResult = false;
					int hypothesisOrAccuse = Integer.valueOf(hypothesisOrAccuseLine);
					if (hypothesisOrAccuse == 1) {
						
						doHypothesis(players.get(playerId), players, mapRestCards, roomMap.get(f)); // do hypothesis
						// showHypothesis(players);

					} else if (hypothesisOrAccuse == 2) {
						accuseResult = doAccuse(players.get(playerId), roomMap.get(f), initialCards); // do accuse
						if (accuseResult) {
							break; // if user do a write accuse game over.else this user will been removed from
									// user list.
						} else {
							delPlayer.add(playerId); //add user into deleted player list
							if (delPlayer.size() + 1 == allPlayerSize) {   //if only one user left game over
								int finalID = 0;
								for (int i = 1; i <= players.size(); i++) {
									if (!delPlayer.contains(i)) {
										finalID = i;
										break;
									}
								}
								// if only one user left game over
								System.out.println();
								// print initial three cards and game info
								System.out.println(
										"▇▇-----------------------     GameOver   -------------------------▇▇");
								System.out.println("▇-- Only One player left");
								System.out.println("Player" + finalID + " win the game");
								System.out.print("Initial Three cards are\t" + "Room: " + roomMap.get(f));

								for (Card c : initialCards) {  // get all initial cards
									if (c.getType().equals("weapon")) {
										System.out.print(" Weapon: " + c.getName());
									} else if (c.getType().equals("suspect")) {
										System.out.print(" Suspect: " + c.getName());
									}
								}

								break;
							}

						}
					}
				} else {
					// room 1 ,6 ,3, 8  can jump to another room
					int jump = 0;
					if (f == 1) {
						jump = 6;
					} else if (f == 6) {
						jump = 1;
					} else if (f == 3) {
						jump = 8;
					} else if (f == 8) {
						jump = 3;
					}

					// if user go to a room located in the corner ,user can choose to jump to
					// another room
					System.out.println("▇-Please choose: 1 do hypothesis, 2 do accuse, 3 Jump To room " + jump + "("
							+ roomMap.get(jump) + ")");
					Scanner sb = new Scanner(System.in);
					System.out.print("△--");
					String hypothesisOrAccuseLine = sb.next();
					// check the valid of user input
					while (!hypothesisOrAccuseLine.equals("1") && !hypothesisOrAccuseLine.equals("2")
							&& !hypothesisOrAccuseLine.equals("3")) {
						System.out.println("▇===== Your input is not valid=====▇ ");
						System.out.println("▇-Please choose: 1 do hypothesis, 2 do accuse, 3 Jump To room " + jump + "("
								+ roomMap.get(jump) + ")");
						hypothesisOrAccuseLine = sb.next();
					}
					boolean accuseResult = false;
					int hypothesisOrAccuse = Integer.valueOf(hypothesisOrAccuseLine);

					if (hypothesisOrAccuse == 1) {
						doHypothesis(players.get(playerId), players, mapRestCards, roomMap.get(f)); // do hypothesis
						// showHypothesis(players);
					} else if (hypothesisOrAccuse == 2) {
						accuseResult = doAccuse(players.get(playerId), roomMap.get(f), initialCards); // do accuse
						if (accuseResult) {
							break;
						} else {
							// romve usr who made a wrong accuse
							delPlayer.add(playerId);     //add user to deleted player list
							if (delPlayer.size() + 1 == allPlayerSize) {     // if only one user left game over.
								int finalID = 0;
								for (int i = 1; i <= players.size(); i++) {
									if (!delPlayer.contains(i)) {
										finalID = i;
										break;
									}
								}
								// if only one user left ,game over
								System.out.println();
								System.out.println(
										"▇▇-----------------------     GameOver   -------------------------▇▇");
								System.out.println("▇-- Only One player left");
								System.out.println("Player" + finalID + " win the game");
								System.out.print("Initial Three cards are\t" + "Room: " + roomMap.get(f));

								for (Card c : initialCards) {      // show all three initial cards
									if (c.getType().equals("weapon")) {
										System.out.print(" Weapon: " + c.getName());
									} else if (c.getType().equals("suspect")) {
										System.out.print(" Suspect: " + c.getName());
									}
								}

								break;
							}

						}
					} else {
						// do jump action from one room to another room

						String line = Board.getDoors(jump);
						String[] sp = line.split(",");
						players.get(playerId).getPawn().setX(Integer.valueOf(sp[0]));  //set coordinate x
						players.get(playerId).getPawn().setY(Integer.valueOf(sp[1]));  // set coordinate y
						Board bd = new Board();
						int[][] bdarray = bd.getBoard();
						resultMV = bdarray[players.get(playerId).getPawn().getX()][players.get(playerId).getPawn()
								.getY()];
						f = resultMV / 100;
						System.out.println("▇ ==============After Jumping===========▇ ");
						// show user position after jumping
						Board board2 = new Board();
						board2.drawBoard(players.get(playerId));
						System.out.println("▇-Now you are in the '" + roomMap.get(f) + "'space");

						// choose do accuse or do hypothesis
						System.out.println("▇-Please choose: 1 do hypothesis, 2 do accuse");
						Scanner sb2 = new Scanner(System.in);
						System.out.print("△--");
						String hypothesisOrAccuseLine2 = sb.next();

						while (!hypothesisOrAccuseLine2.equals("1") && !hypothesisOrAccuseLine2.equals("2")) { // check
																												// the
																												// valid
																												// of
																												// user
																												// input
							System.out.println("▇===== Your input is not valid=====▇ ");
							System.out.println("Please choose: 1 do hypothesis, 2 do accuse");
							hypothesisOrAccuseLine2 = sb.next();
						}
						boolean accuseResult2 = false;
						int hypothesisOrAccuse2 = Integer.valueOf(hypothesisOrAccuseLine2);
						if (hypothesisOrAccuse2 == 1) {
							doHypothesis(players.get(playerId), players, mapRestCards, roomMap.get(f)); // do hypothesis
							// showHypothesis(players);

						} else if (hypothesisOrAccuse2 == 2) {
							accuseResult2 = doAccuse(players.get(playerId), roomMap.get(f), initialCards); // do accuse
							if (accuseResult2) {
								break;
							} else {
								
								//remove user who made wrong accusation
								delPlayer.add(playerId);
								if (delPlayer.size() + 1 == allPlayerSize) {
									int finalID = 0;
									for (int i = 1; i <= players.size(); i++) {
										if (!delPlayer.contains(i)) {
											finalID = i;
											break;
										}
									}
									System.out.println();
									
									// game over show all initial cards and rest game info
									System.out.println(
											"▇▇-----------------------     GameOver   -------------------------▇▇");
									System.out.println("▇-- Only One player left");
									System.out.println("Player" + finalID + " win the game");
									System.out.print("Initial Three cards are\t" + "Room: " + roomMap.get(f));

									for (Card c : initialCards) {  //get initial cards
										if (c.getType().equals("weapon")) {
											System.out.print(" Weapon: " + c.getName());
										} else if (c.getType().equals("suspect")) {
											System.out.print(" Suspect: " + c.getName());
										}
									}

									break;
								}

							}
						}

					}

				}

			}
			//choose get notebook ,get cards or continue to next player
			Scanner ss = new Scanner(System.in);
			System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards  3-Continue");
			System.out.print("△--");
			norPLine = ss.next();
			// check the valid of user input
			while (!checkInteger(norPLine, 1, 3)) {
				System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards  3-Continue");
				System.out.print("△--");
				norPLine = ss.next();
			}
			norP = Integer.valueOf(norPLine);

			while (norP != 3) {
				if (norP == 1) {
					showMyNoteBook(players.get(playerId)); //show all my notebook
				} else {
					showMyCards(players.get(playerId));   // show all my cards
				}
				System.out.println("Please choose: 1-Get my notebook   2-Get my cards   3-Continue");
				System.out.print("△--");
				norPLine = ss.next();

				while (!checkInteger(norPLine, 1, 3)) {  //check the valid of user input
					System.out.println("▇-Please choose: 1-Get my notebook   2-Get my cards  3-Continue");
					System.out.print("△--");
					norPLine = ss.next();
				}
				norP = Integer.valueOf(norPLine);
			}

			System.out.println("▇-Player" + (playerId) + " round finish");

			
			//  choose the ID of next player .  if current player is the last player, next player is the first player
			for (int i = playerId; i <= allPlayerSize; i++) {
				int x = i + 1;
				if (x > allPlayerSize) {
					x = 1;
					i = 0;
				}
				if (delPlayer.contains(x)) {   //if current user have recorded in deleted player list, ignore current user.
					continue;
				} else {
					playerId = x;
					break;
				}

			}
			System.out.println("★ ---------- Press any with Enter to continue ----------★");
			sc.next();
			clearL();

		}

	}
// Method used to check the valid of user input
	public boolean checkInteger(String line, int i, int j) {
		try {
			int num = Integer.valueOf(line);// 把字符串强制转换为数字
			if (num >= i && num <= j) {
				return true;// 如果是数字，返回True
			}
			System.out.println("▇===== Your input is not valid=====▇ ");
			return false;
		} catch (Exception e) {
			System.out.println("▇===== Your input is not valid=====▇ ");
			return false;// 如果抛出异常，返回False}
		}
	}
	// Method used to check the valid of user input
	public boolean checkInteger2(String line, int i, int j) {
		try {
			int num = Integer.valueOf(line); // transer user input from string to integer
			if (num >= i && num <= j) {
				return true;
			}

			return false;
		} catch (Exception e) {

			return false;
		}
	}
//Method used to show cards of a player
	public void showMyCards(Player player) {

		System.out.println();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("▇-Player" + player.getId() + "'s cards ");
		List<String> roomlist = player.getNotebook().getRooms(); //get room card list
		if (roomlist == null) {
			roomlist = new ArrayList<String>();
			System.out.println("Room : No Room Cards ");
		} else {
			System.out.print("Room : ");
			for (String s : roomlist) {

				System.out.print(s + ",  ");     //print all current user room cards.
			}
			System.out.println();
		}

		List<String> suspectlist = player.getNotebook().getSuspects();  //get suspecrts cards list
		if (suspectlist == null) {
			suspectlist = new ArrayList<String>();
			System.out.println("Suspects: No Suspects Cards");
		} else {
			System.out.print("Suspects : ");
			for (String s : suspectlist) {
				System.out.print(s + ",  ");  //print all current user Suspects cards.
			}
			System.out.println();
		}

		List<String> weaponlist = player.getNotebook().getWeapons();  //get weapon cards list
		if (weaponlist == null) {
			weaponlist = new ArrayList<String>();
			System.out.println("Weapons: No Weapons Cards");
		} else {
			System.out.print("Weapons : ");
			for (String s : weaponlist) {

				System.out.print(s + ",  ");   //print all current user Weapons cards.
			}
			System.out.println();
		}

		System.out.println("-------------------------------------------------------------------");

	}
//Method used to show player notebook
	public void showMyNoteBook(Player player) {

		System.out.println();
		System.out.println("-------------------------------------------------------------------");
		System.out.println("▇-Player" + player.getId() + "'s notebook ");
		List<String> list = player.getNotebook().getHypothesis();  
		if (list == null) {
			System.out.println("No content in the notebook");
		} else {
			for (String s : list) {
				System.out.println(s);  // print current user notebook
			}
		}

		System.out.println("-------------------------------------------------------------------");

	}
  //Method used to implement accuse logic
	public boolean doAccuse(Player player, String roomNow, List<Card> initialCards) {

		System.out.println("▇-player" + player.getId() + " is formulating a  accusation:");
		showMyCards(player);
		System.out.println("▇-ALL cards are list here");
		Map<String, String[]> allCards = Cards.getAllCards();  //get all cards list
		Set cardsType = allCards.keySet();
		// print all cards info
		for (Object o : cardsType) {
			if (o.toString().equals("ROOMS")) {
				System.out.println("ROOMS: 1-" + roomNow);
				continue;
			}
			String[] cardList = allCards.get(o.toString());
			System.out.print(o.toString() + ": ");
			for (int i = 1; i <= cardList.length; i++) {

				System.out.print(i + "-" + cardList[i - 1] + ",  ");
			}
			System.out.println();
		}

		System.out.println("▇-Please input the NO. of the cards from different Type; Seperate by comma ");
		System.out.print("△--");
		Scanner sc = new Scanner(System.in);
		String[] nos = sc.next().split(",");
		//check the valid of user input
		while (nos.length != 3) {
			System.out.println("▇===== Your input is not valid=====▇ ");
			sc = new Scanner(System.in);
			nos = sc.next().split(",");
		}

		String[] weapons = allCards.get("WEAPONS");  //get  weapons card list
		String[] suspect = allCards.get("SUSPECTS");  //get suspects card list

		// check valid of user input
		while (!checkInteger2(nos[0], 1, 1) || !checkInteger2(nos[1], 1, suspect.length)
				|| !checkInteger2(nos[2], 1, weapons.length)) {
			System.out.println("▇===== Your input is not valid=====▇ ");
			sc = new Scanner(System.in);
			System.out.print("△--");
			nos = sc.next().split(",");

		}

		boolean result = checkAccussion(weapons[Integer.valueOf(nos[2]) - 1], suspect[Integer.valueOf(nos[1]) - 1],
				roomNow, initialCards);  //check accussion is right or wrong

		if (result) {  // if accussion is right game over.
			System.out.println("GameOver");
			System.out.println("Player" + player.getId() + " win the game");
			System.out.println("Initial Three cards are\t" + "Room:" + roomNow + "  Weapon:"  //print initial three cards
					+ weapons[Integer.valueOf(nos[2]) - 1] + "  Suspect" + suspect[Integer.valueOf(nos[1]) - 1]);

		} else {
			// if accussion is wrong  game over
			System.out.println("Player" + player.getId() + "made a wrong accussion"); 
			System.out.println("Player" + player.getId() + "out");  // if accussion is wrong , current user removed from game.
		}

		return result;

	}
	 //check accussion is right or wrong
	public boolean checkAccussion(String weapon, String suspect, String roomNow, List<Card> initialCards) {

		
		//compare the accussion cards with intital three cards
		for (Card c : initialCards) {
			if (c.getType().equals("weapon") && c.getName().equals(weapon)) {   //compare with initial weapon cards
				continue;
			} else if (c.getType().equals("suspect") && c.getName().equals(suspect)) { //compare with initial suspect cards
				continue;
			} else if (c.getType().equals("room") && c.getName().equals(roomNow)) { //compare with initial room cards
				continue;
			}
			return false;
		}
		return true;

	}
//  Method used to show all hypothesis
	public void showHypothesis(Map<Integer, Player> players) {

		for (int i = 1; i <= players.size(); i++) {

			Player player = players.get(i);
			List<String> hp = player.getNotebook().getHypothesis();

			System.out.println("Player" + player.getId() + "-------------------");
			for (String s : hp) {
				System.out.println(s);
			}

		}
	}
// Method used to do hypothesis
	public void doHypothesis(Player player, Map<Integer, Player> players, Map<String, List<String>> mapRestCards,
			String roomNow) {

		System.out.println("player" + player.getId() + " is formulating a  hypothesis");

		showMyCards(player);  // show all cards of curren player

		System.out.println("▇-ALL cards are list here");
		Map<String, String[]> allCards = Cards.getAllCards();  // get all available cards
		Set cardsType = allCards.keySet();    //iterator print all cards info
		for (Object o : cardsType) {
			if (o.toString().equals("ROOMS")) {
				System.out.println("ROOMS: 1-" + roomNow);
				continue;
			}
			String[] cardList = allCards.get(o.toString());
			System.out.print(o.toString() + ": ");
			for (int i = 1; i <= cardList.length; i++) {

				System.out.print(i + "-" + cardList[i - 1] + ",  ");
			}
			System.out.println();
		}
      // get the input of cards No.
		System.out.println("▇-Please input the NO. of the cards from different Type; Seperate by comma ");
		System.out.print("△--");
		Scanner sr = new Scanner(System.in);
		String la = sr.next();
		String[] nos = la.split(",");
		// check the valid of user input
		while (nos.length != 3) {
			System.out.println("▇===== Your input is not valid=====▇ ");
			sr = new Scanner(System.in);
			la = sr.next();
			nos = la.split(",");
		}

		String[] weapons = allCards.get("WEAPONS"); //get  weapons card list
		String[] suspect = allCards.get("SUSPECTS");//get  SUSPECTS card list

		// String suspect = allCards.get("SUSPECTS").get(Integer.valueOf(nos[2])-1);

		while (!checkInteger2(nos[0], 1, 1) || !checkInteger2(nos[1], 1, suspect.length)   //check valid of user input
				|| !checkInteger2(nos[2], 1, weapons.length)) {
			System.out.println("▇===== Your input is not valid=====▇ ");
			sr = new Scanner(System.in);
			System.out.print("△--");
			la = sr.next();
			nos = la.split(",");
		}
		//check hypothese is right or wrong.
		String hypothesisResult = checkHypothesis(weapons[Integer.valueOf(nos[2]) - 1], 
				suspect[Integer.valueOf(nos[1]) - 1], roomNow, player, players);
				
		// add realtive information to player note book
		if (hypothesisResult.equals("SUCCESS")) {
			System.out.println("▇-Your Hypothesis is right");

			String messageMe = "SUCCESS: I formulated the hypothesis that ”" + suspect[Integer.valueOf(nos[1]) - 1]
					+ "“ made the murder in the ”" + roomNow + "“ with the ”" + weapons[Integer.valueOf(nos[2]) - 1]
					+ "“\n";     //create notebook message of current player
			System.out.println(messageMe);

			String messageOther = "SUCCESS: Player" + player.getId()
					+ " formulated the hypothesis that **** made the murder in the ”" + roomNow + "“ with the ***.\n";
			 //create notebook message of other player
			// update notebook of each player
			for (int i = 0; i < players.size(); i++) {
				Player p = players.get(i+1);
				Notebooks nb = p.getNotebook();
				List<String> hypothesis = nb.getHypothesis();
				if (hypothesis == null) {
					hypothesis = new ArrayList<String>();
				}
				if (p.getId() == player.getId()) {
					hypothesis.add(messageMe);
				} else {
					hypothesis.add(messageOther);
				}
				nb.setHypothesis(hypothesis);
			}

		} else {
			// if player do a wrong hypothsis
			String[] hResult = hypothesisResult.split(":");
			String type = hResult[0]; //get type of refute card
			String name = hResult[1];//get name of refute card
			String pID = hResult[2];  //get playerId of player who refute the hypothesis
            //create note book message of each player
			System.out.println("▇-Your Hypothesis is wrong");

			String messageMe1 = "Fail: I formulated the hypothesis that “" + suspect[Integer.valueOf(nos[1]) - 1]
					+ "” made the murder in the “" + roomNow + "” with the “" + weapons[Integer.valueOf(nos[2]) - 1]
					+ "“";
			;
			String messageMe2 = "Fail: Player" + pID + " refuted the hypothesis showing the card “" + name + "”. \n";

			String messageRefute1 = "Refute: Player" + player.getId() + " formulated the hypothesis that ”"
					+ suspect[Integer.valueOf(nos[1]) - 1] + "“ made the murder in the ”" + roomNow + "“ with the ”"
					+ weapons[Integer.valueOf(nos[2]) - 1] + "”";
			;
			String messageRefute2 = "Refute: I refuted the hypothesis showing card “" + name + "”.\n";

			String messageOther1 = "Other: Player" + player.getId() + " made the hypothesis that “"
					+ suspect[Integer.valueOf(nos[1]) - 1] + "” made the murder in the “" + roomNow + "” with the “"
					+ weapons[Integer.valueOf(nos[2]) - 1] + "“";
			String messageOther2 = "Other: Player" + pID + " refuted the hypothesis showing a card\n";

			// update notebook for each player
			for (int i = 0; i < players.size(); i++) {
				Player p = players.get(i+1);
				Notebooks nb = p.getNotebook();
				List<String> hypothesis = nb.getHypothesis();
				if (hypothesis == null) {
					hypothesis = new ArrayList<String>();
				}
				if (p.getId() == player.getId()) {
					hypothesis.add(messageMe1);
					hypothesis.add(messageMe2);
				} else if (p.getId() == Integer.valueOf(pID)) {
					hypothesis.add(messageRefute1);
					hypothesis.add(messageRefute2);
				} else {
					hypothesis.add(messageOther1);
					hypothesis.add(messageOther2);
				}
				nb.setHypothesis(hypothesis);
			}

		}

	}
  //Method used to check hypothesis
	public String checkHypothesis(String weapon, String suspect, String roomNow, Player player,
			Map<Integer, Player> players) {
//		At this point the system checks - starting from the player who played
//		immediately before the current one – whether s/he holds at least a card equal to the one used
//		for the hypothesis (character, room or weapon) that can refute the hypothesis. If not the
//		process continues to the previous player until either one player disproves the suggestion, or no
//		one can do so.
		 
		int playerID = player.getId();
		int i = playerID - 1;
		if (i < 1) {
			i = players.size();
		}
		
		for (; i >= 0; i--) {

			if (i < 1) {
				i = players.size();

			}
			Notebooks nb = players.get(i).getNotebook();
			List<String> roomCard = nb.getRooms();
			if (roomCard != null && roomCard.contains(roomNow))
				return "room:" + roomNow + ":" + i;

			List<String> suspectCard = nb.getSuspects();
			if (suspectCard != null && suspectCard.contains(suspect))
				return "suspect:" + suspect + ":" + i;

			List<String> weaponCard = nb.getWeapons();
			if (weaponCard != null && weaponCard.contains(weapon))
				return "weapon:" + weapon + ":" + i;
			if (i == playerID)
				break;
		}

		return "SUCCESS";
	}

	// get the input of user moving
	public int judgeMove() {
		System.out.println("▇-Do you want to move?  1: Yes \t 2:No");
		int mv;

		System.out.print("△--");
		String x = sc.next();
		// test the valid of user input
		if (x.equals("1") || x.equals("2")) {
			mv = Integer.valueOf(x);
		} else {
			mv = 5;
		}
		return mv;
	}

	// do the move logic of user pawn
	public int move(int total, Player player) {

		System.out.println("▇=Please input move step ");
		System.out.println("▇=up: u ; down: d ; left: l ; right: r     Seperate by comma");
		System.out.print("△--");
		Scanner sv = new Scanner(System.in);
		String ss = sv.next();

		String[] steps = ss.split(","); // split the input of user
		int tn = 0;
		if (steps.length == 0)
			return -1;
		int tag = 0;
		for (String s : steps) { // test the vlaid of each step

			try {
				char ch = s.charAt(0); // get the direction
				int num = Integer.valueOf(s.substring(1)); // get the num of step
				tn += num;

				if (tn > total) // if the input of user more than valid step numbers return -1;
					return -1;
				int result = judgeMoveValid(ch, num, player, tag++); // test the valid of user input

				if (result != 1) {
					return result;
				}
			} catch (Exception e) {
				return -1;
			}
		}

		return 1; // 1 valid movement ,2 reach door position, -1 invalid;
	}

	// Method used to test the valid of user pawn movement
	public int judgeMoveValid(char ch, int num, Player player, int tag) {

		Board board = new Board();
		int[][] bd = board.getBoard();
		// get initial location of user pawn
		int x = player.getPawn().getX();
		int y = player.getPawn().getY();

		// test each step is valid or not
		if (ch == 'u') {
			if (bd[x][y] > 100) {
				return -1;
			}

			int st = x;
			for (int i = 1; i <= num; i++) {
				st = x - i;
				if (bd[st][y] == 2 || bd[st][y] == 1 || bd[st][y] == 3) // 1,2 is border
					return -1;
				if (bd[st][y] > 100) { // the valud of door location is greater than 100
					player.getPawn().setX(st); // moving upwards only need to change row.
					return bd[st][y];
				}

			}

			player.getPawn().setX(st);
			return 1;

		} else if (ch == 'd') {
			if (bd[x][y] > 100) {
				return -1;
			}
			int st = x;
			for (int i = 1; i <= num; i++) {
				st = x + i;
				if (bd[st][y] == 2 || bd[st][y] == 1 || bd[st][y] == 3)
					return -1;
				if (bd[st][y] > 100 && i != 1) {
					player.getPawn().setX(st); // moving downward only need to change row.
					return bd[st][y];
				}
			}

			player.getPawn().setX(st);
			return 1;

		} else if (ch == 'l') {

			int st = y;
			for (int i = 1; i <= num; i++) {
				st = y - i;
				if (bd[x][st] == 2 || bd[x][st] == 1 || bd[x][st] == 3)
					return -1;
				if (bd[x][st] > 100) {
					player.getPawn().setY(st); // moving left only need to change column.
					return bd[x][st];
				}
			}

			player.getPawn().setY(st);
			return 1;

		} else if (ch == 'r') {

			int st = y;
			for (int i = 1; i <= num; i++) {
				st = y + i;
				if (bd[x][st] == 2 || bd[x][st] == 1 || bd[x][st] == 3)
					return -1;
				if (bd[x][st] > 100) {
					player.getPawn().setY(st); // moving right only need to change column.
					return bd[x][st];
				}
			}

			player.getPawn().setY(st);
			return 1;

		}
		return -1;
	}

	// method used to play dicing
	public int playDice() {

		System.out.println("▇=Dicing:-------------");
		try {
			Thread.sleep(1000L);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int diceOne = (int) (Math.random() * 5) + 1; // create two random integer
		int diceTwo = (int) (Math.random() * 5) + 1;
		int total = diceOne + diceTwo; // get the sum of this two integer
	//	int total = 20;
		System.out.print("▇=diceOne is\t" + diceOne);
		System.out.println("\tdiceTwo is\t" + diceTwo);
		System.out.println("▇=You can move " + total + " steps");

		return total;
	}
//Method used to clear console
	public void clearL() {
		Robot r;
		try {
			r = new Robot();
			r.mousePress(InputEvent.BUTTON3_MASK); // 按下鼠标右键
			r.mouseRelease(InputEvent.BUTTON3_MASK); // 释放鼠标右键
			r.keyPress(KeyEvent.VK_CONTROL); // 按下Ctrl键
			r.keyPress(KeyEvent.VK_R); // 按下R键
			r.keyRelease(KeyEvent.VK_R); // 释放R键
			r.keyRelease(KeyEvent.VK_CONTROL); // 释放Ctrl键
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
