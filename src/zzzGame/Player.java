package zzzGame;

import dom.Notebooks;
import dom.Pawns;
// Plaer class used to wrap play basic info
public class Player {
	private int id;  //player id
	private String name;//player name
	private Pawns  pawn; // pawn used to record user location
	private Notebooks notebook = new Notebooks();   //player notebook
	
	
	public Player(int id, String name) {
		this.id = id;
		this.name = name;
	   this.pawn = new Pawns("suspect",name,8,21);
	
	}
	
	
	public Pawns getPawn() {
		return pawn;
	}


	public void setSuspectPawn(Pawns suspectPawn) {
		this.pawn = suspectPawn;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Notebooks getNotebook() {
		return notebook;
	}
	public void setNotebook(Notebooks notebook) {
		this.notebook = notebook;
	}
	
	
	
	
}
